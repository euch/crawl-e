CREATE TABLE public.news
(
  id bigint NOT NULL,
  link text,
  save_date timestamp with time zone,
  title text NOT NULL,
  news_source_parent_fk bigint,
  content text,
  pub_date timestamp with time zone,
  CONSTRAINT news_pkey PRIMARY KEY (id),
  CONSTRAINT fk_kb0d9sac90llnqdn5re01uhb4 FOREIGN KEY (news_source_parent_fk)
      REFERENCES public.news_source (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT news_link_title_content_key UNIQUE (link, title, content)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.news
  OWNER TO postgres;

CREATE TABLE public.news_source
(
  id bigint NOT NULL,
  url character varying(255) NOT NULL,
  enabled boolean NOT NULL DEFAULT true,
  name character varying NOT NULL DEFAULT true,
  mirror_content boolean NOT NULL DEFAULT true,
  CONSTRAINT news_source_pkey PRIMARY KEY (id),
  CONSTRAINT uk_9c5puj5k7lt8ycnqg1381e9b8 UNIQUE (url)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.news_source
  OWNER TO postgres;

CREATE TABLE public.ottd_source
(
  id bigint NOT NULL,
  name character varying(255) NOT NULL,
  enabled boolean NOT NULL DEFAULT true,
  CONSTRAINT ottd_source_pkey PRIMARY KEY (id),
  CONSTRAINT ottd_source_unique_name UNIQUE (name)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.ottd_source
  OWNER TO postgres;

CREATE TABLE public.ottd_status
(
  id bigint NOT NULL,
  probe_date timestamp with time zone NOT NULL,
  online boolean,
  companies_online smallint,
  players_online smallint,
  game_date date,
  ottd_source bigint NOT NULL,
  CONSTRAINT ottd_status_pkey PRIMARY KEY (id),
  CONSTRAINT ottd_source_parent_fk FOREIGN KEY (ottd_source)
      REFERENCES public.ottd_source (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.ottd_status
  OWNER TO postgres;
