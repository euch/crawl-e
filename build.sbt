name := """crawl-e"""

version := "1.0"

scalaVersion := "2.11.6"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % "2.3.11",
  "com.typesafe.akka" %% "akka-testkit" % "2.3.11" % "test",
  "org.scalatest" %% "scalatest" % "2.2.4" % "test",
  "org.scalikejdbc" %% "scalikejdbc"        % "2.5.+",
//  "ch.qos.logback"  %  "logback-classic"    % "1.1.+",
  "org.postgresql" % "postgresql" % "9.4.1212",
  "com.twitter" % "finagle-http_2.11" % "6.41.0",
  "org.scala-lang" % "scala-xml" % "2.11.0-M4",
  "net.sourceforge.htmlcleaner" % "htmlcleaner" % "2.18"
)
