/*
package com.crawle
import java.time.LocalDate

import com.crawle.Model.Ottd
import org.scalatest.FunSuite
import akka.actor.ActorSystem
import akka.actor.Actor
import akka.actor.Props
import akka.testkit.{ImplicitSender, TestActors, TestKit}
import org.scalatest.WordSpecLike
import org.scalatest.Matchers
import org.scalatest.BeforeAndAfterAll
import scala.concurrent.duration._

class OttdMonDBTest  extends FunSuite {
    test("Check if Ottd source is fetching properly") {
      assert(Ottd.allEnabledSources.nonEmpty)
      assert(Ottd.allEnabledSources.head.name == "Russia.Samara ECS")
    }
}

class OttdMonWorkerTest(_system: ActorSystem) extends TestKit(_system) with ImplicitSender
  with WordSpecLike with Matchers with BeforeAndAfterAll {

  def this() = this(ActorSystem("MySpec"))

  override def afterAll {
    TestKit.shutdownActorSystem(system)
  }

  "A Worker actor" must {
    "send back Ottd status report" in {
      system.actorOf(Props(new Worker(Ottd.allEnabledSources.head)))
      expectMsg(max = 10 seconds, obj = Ottd.Status(online = true, Some(0), Some(0), Some(LocalDate.now())))
    }
  }

}*/
