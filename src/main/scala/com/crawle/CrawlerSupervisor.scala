package com.crawle

import java.util.concurrent.TimeUnit

import akka.actor.{Actor, ActorLogging, Props}
import Model.{News, Ottd, SourceTrait}
import com.crawle.Worker.Kill

import scala.concurrent.Future
import scala.concurrent.duration.Duration
import scala.util.{Failure, Success}

class CrawlerSupervisor extends Actor with ActorLogging {

  import CrawlerSupervisor._

  import scala.concurrent.ExecutionContext.Implicits.global

  val createSourceBaseInterval = Duration.create(1, TimeUnit.MINUTES)
  def workerCreationIntervalMillis(toCreateCount: Int) = createSourceBaseInterval.toMillis / toCreateCount

  val checkSourceInterval = Duration.create(1, TimeUnit.MINUTES)

  var currentSources = Set[SourceTrait]()


  def workerName(id: Long) = s"worker-$id"

  execNewsUpdate(Future({
    currentSources = fetchSources
    createUpdateWorkers(currentSources)
  }))

  def execNewsUpdate[T](body: ⇒ Future[T]) = body.onComplete {
    case Success(value) => context.system.scheduler.scheduleOnce(checkSourceInterval, self, RefreshNewsSources)
    case Failure(e) => e.printStackTrace()
  }

  def fetchSources = News.fetchAllEnabledSources ++ Ottd.fetchAllEnabledSources

  def receive = {
    case RefreshNewsSources =>
      execNewsUpdate(Future({
        val fresh = fetchSources
        deleteWorkers(currentSources.map(_.id).diff(fresh.map(_.id)))
        createUpdateWorkers(fresh.diff(currentSources))
        currentSources = fresh
      }))
    case news: NewsEntriesToSave =>
      news.entries.foreach(e => News.saveNews(e, news.newsSource))
    case ottdStatus: OttdStatusToSave =>
      Ottd.saveStatus(ottdStatus.status, ottdStatus.source)
    case wtf => log.error(s"invalid msg: $wtf")
  }

  def deleteWorkers(newsSourceIds: Set[Long]) = newsSourceIds.foreach(nsi => {
    val wn = workerName(nsi)
    context.child(wn) match {
      case None => log.warning(s"No actor to kill: $wn")
      case Some(w) => w ! Kill
    }
  })

  def createUpdateWorkers(newsSources: Set[SourceTrait]) {
    newsSources.headOption match {
      case None =>
      case Some(ns) =>
        context.child(workerName(ns.id)) match {
          case None => context.actorOf(Props(new Worker(ns)), name = workerName(ns.id))
          case Some(w) => w ! ns
        }
        Thread.sleep(workerCreationIntervalMillis(newsSources.size))
        createUpdateWorkers(newsSources.filterNot(_ == ns))
    }
  }

}

object CrawlerSupervisor {

  case class NewsEntriesToSave(entries: Seq[News.Record], newsSource: News.Source)

  case class OttdStatusToSave(status: Ottd.Status, source: Ottd.Source)

  case class CreateUpdateWorkers(newsSources: Set[News.Source])

  case object Initialize

  case object RefreshNewsSources

}