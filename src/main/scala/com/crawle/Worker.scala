package com.crawle

import java.io.IOException
import java.net.URL
import java.time.format.DateTimeFormatter
import java.time.{LocalDate, ZonedDateTime}
import java.util.concurrent.TimeUnit

import akka.actor.{Actor, ActorLogging}
import com.crawle.CrawlerSupervisor.{NewsEntriesToSave, OttdStatusToSave}
import com.crawle.Model.{News, Ottd, SourceTrait}
import com.crawle.Worker.{Check, Kill}
import org.apache.commons.lang.StringEscapeUtils
import org.htmlcleaner.{ContentNode, HtmlCleaner, TagNode}

import scala.concurrent.duration.Duration
import scala.xml.{Node, SAXParseException, XML}

class Worker(var source: SourceTrait) extends Actor with ActorLogging {

  var checkInterval = Duration.create(1, TimeUnit.MINUTES)
  var initialDelay = Duration.create(5, TimeUnit.SECONDS)

  import scala.concurrent.ExecutionContext.Implicits.global

  log.info(s"created worker for $source")
  context.system.scheduler.schedule(initialDelay, checkInterval, self, Check)
  var latestDate = 0L

  def receive = {
    case s: SourceTrait =>
      source = s
      log.info(s"updated worker for $source")
    case Kill =>
      context.stop(self)
      log.info(s"killed worker for $source")
    case Check =>
      source match {
        case ns: News.Source =>
          checkFeed(ns)
        case ts: Ottd.Source =>
          checkOttd(ts)
      }
  }

  private def checkOttd(src: Ottd.Source) {
    val cleaner = new HtmlCleaner
    context.parent ! OttdStatusToSave(status, src)

    // 1
    def url = try {
      cleaner.clean(new URL("https://www.openttd.org/en/servers")).getElementsByName("tr", true)
        .filter(elem => StringEscapeUtils.unescapeHtml(elem.getText.toString).contains(src.name))
        .map(elem => "https://www.openttd.org/" + elem.getAllChildren.get(3).asInstanceOf[TagNode].getAllChildren.get(0).asInstanceOf[TagNode].getAllChildren.get(0).asInstanceOf[TagNode].getAttributeByName("href"))
        .headOption
    } catch {
      case th: Throwable =>
        println(s"failed to get openttd server page ${th.getMessage}")
        None
    }

    //2
    def status: Ottd.Status = {
      if (url.isDefined) try {
        val elements = cleaner.clean(new URL(url.get)).getElementsByName("tr", true).toList

        def r(name: String): Option[String] = elements.filter(_.getAllChildren.get(0) match {
          case tn: TagNode =>
            tn.getAllChildren.get(0) match {
              case cn: ContentNode =>
                cn.getContent.equals(name)
              case _ =>
                false
            }
          case _ =>
            false
        }).map(_.getAllChildren.get(1).asInstanceOf[TagNode].getText.toString).headOption

        val date = r("Current date:") match {
          case None =>
            None
          case Some(d) =>
            Some(LocalDate.parse(d))
        }
        val (clients, companies) = r("Clients:") match {
          case None =>
            (None, None)
          case Some(c) =>
            val a = c.split("[^\\d]").toSeq.flatMap(toInt)
            (Some(a(0)), Some(a(2)))
        }

        return Ottd.Status(online = true, companies, clients, date)
      } catch {
        case th: Throwable =>
          println(s"failed to parse openttd server page ${th.getMessage}")
      }
      return Ottd.Status(online = false, None, None, None)
    }
  }

  private def toInt(s: String): Option[Int] = {
    try {
      Some(Integer.parseInt(s.trim))
    } catch {
      case e: Exception => None
    }
  }

  def checkFeed(ns: News.Source) = try {
    def buildRssRecord(node: Node) = {
      def t(f: String) = (node \\ f).text

      News.Record(
        title = t("title"),
        link = t("link"),
        content = t("description"),
        pubDate = ZonedDateTime.parse(t("pubDate"), DateTimeFormatter.RFC_1123_DATE_TIME).toInstant
      )
    }

    val n = (XML.load(ns.url) \\ "item").map(buildRssRecord).filter(_.pubDate.toEpochMilli >= latestDate) // not == to prevent rare collisions
    n.reverse.headOption match {
      case None =>
      case Some(ho) =>
        context.parent ! NewsEntriesToSave(n, ns)
        latestDate = ho.pubDate.toEpochMilli
    }
  } catch {
    case e@(_: IOException | _: SAXParseException) => log.warning(s"${e.getLocalizedMessage} $source")
    case th: Throwable => th.printStackTrace()
  }

}


object Worker {

  case object Kill

  case object Check

}