package com.crawle

import akka.actor.{ActorSystem, Props}

object ApplicationMain extends App {
  val system = ActorSystem("CrawleSystem").actorOf(Props[CrawlerSupervisor], "crawlerSupervisor")
}