package com.crawle

import java.time.{Instant, LocalDate, ZoneOffset, ZonedDateTime}

import org.postgresql.util.PSQLException
import scalikejdbc._

object Model {
  Class.forName("org.postgresql.Driver")
  ConnectionPool.singleton("jdbc:postgresql://192.168.1.47:5432/homepage-lite", "postgres", "postgres")

  implicit val session = AutoSession

  sealed trait SourceTrait {
    val id: Long
    val name: String
    val enabled: Boolean
  }

  private def saveDate = ZonedDateTime.now(ZoneOffset.UTC)

  object News {

    def fetchAllEnabledSources: Set[SourceTrait] = sql"select * from news_source where enabled is true".map(rs => Source(rs)).list.apply().toSet

    def saveNews(r: Record, ns: Source): Unit = try {
      val content = if (ns.mirrorContent) r.content
      sql"INSERT INTO public.news(id, link, save_date, title, news_source_parent_fk, content, pub_date) VALUES (nextval('default_sequence'), ${r.link}, $saveDate, ${r.title}, ${ns.id}, $content, ${r.pubDate}) ".update.apply()
    } catch {
      case err: PSQLException if err.getMessage.contains("""duplicate key value violates unique constraint "news_link_title_content_key"""") =>
      case th: Throwable =>
        th.printStackTrace()
    }

    case class Record(link: String, title: String, content: String, pubDate: Instant)

    case class Source(override val id: Long,
                      url: String,
                      override val enabled: Boolean,
                      mirrorContent: Boolean,
                      override val name: String) extends SourceTrait

    object Source extends SQLSyntaxSupport[Source] {
      override val tableName = "news_source"

      def apply(rs: WrappedResultSet): Source = new Source(
        rs.long("id"), rs.string("url"), rs.boolean("mirror_content"), rs.boolean("enabled"), rs.string("name"))
    }

  }

  object Ottd {
    def fetchAllEnabledSources: Set[SourceTrait] = sql"select * from ottd_source where enabled is true".map(rs => Source(rs)).list.apply().toSet

    def saveStatus(st: Status, src: Source): Unit = try {
       val saveDate = Instant.now
       sql"INSERT INTO public.ottd_status(id, probe_date, online, companies_online, players_online, game_date, ottd_source) VALUES (nextval('default_sequence'), $saveDate, ${st.online}, ${st.companies}, ${st.players}, ${st.date}, ${src.id}) ".update.apply()
     } catch {
       case err: PSQLException =>
       case th: Throwable =>
         th.printStackTrace()
     }

    case class Status(online: Boolean, companies: Option[Int], players: Option[Int], date: Option[LocalDate])

    case class Source(override val id: Long, override val name: String, override val enabled: Boolean) extends SourceTrait

    object Source extends SQLSyntaxSupport[Source] {
      override val tableName = "ottd_source"

      def apply(rs: WrappedResultSet): Source = new Source(rs.long("id"), rs.string("name"), rs.boolean("enabled"))
    }

  }

}
